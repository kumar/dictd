# translation of dictd debconf to Portuguese
# Copyright (C) 2009 the dictd's copyright holder
# This file is distributed under the same license as the dictd package.
#
# Américo Monteiro <a_monteiro@netcabo.pt>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: dictd 1.11.1+dfsg-2\n"
"Report-Msgid-Bugs-To: dictd@packages.debian.org\n"
"POT-Creation-Date: 2009-04-06 07:52+0200\n"
"PO-Revision-Date: 2009-04-06 12:52+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@netcabo.pt>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: select
#. Choices
#: ../dictd.templates:2001
msgid "daemon"
msgstr "daemon"

#. Type: select
#. Choices
#: ../dictd.templates:2001
msgid "inetd"
msgstr "inetd"

#. Type: select
#. Choices
#: ../dictd.templates:2001
msgid "disabled"
msgstr "desactivado"

#. Type: select
#. Description
#: ../dictd.templates:2002
msgid "Method for running dictd:"
msgstr "Método de funcionamento do dictd:"

#. Type: select
#. Description
#: ../dictd.templates:2002
msgid ""
"The dictd server can be run either as a stand-alone daemon or from inetd. "
"You can also disable it entirely."
msgstr ""
"O servidor dictd pode funcionar como um deamon autónomo ou a partir do "
"inetd. Também o pode desactivar completamente."

#. Type: select
#. Description
#: ../dictd.templates:2002
msgid "It is recommended to run it as a daemon."
msgstr "É recomendado corrê-lo como um deamon."
