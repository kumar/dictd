     Because the dictd daemon has a significant startup time, the
author felt it should not be started from inetd.  However, the fast
processors that are commonly used today make it feasible to start
dictd from inetd.


     On Debian systems this can be accomplished by running as the
root user the following command:
	dpkg-reconfigure dictd
and selecting "inetd" option.  The command will update the $RUN_MODE
variable in /etc/default/dictd file, and add the following line into
/etc/inetd.conf:

dict  stream  tcp  nowait  dictd.dictd  /usr/sbin/tcpd  /usr/sbin/dictd $DICTD_ARGS --inetd

where $DICTD_ARGS will be replaced with the actual value defined in
/etc/default/dictd file.  It will also try to apply the changes into
the system by stopping running dictd daemon, and reloading inetd
configuration.


	Please note that the system access controls must allow the dictd
daemon to be accessed by processes on the local host.  If /etc/hosts.deny
restricts access, then /etc/hosts.allow must provide it.  `dictd:localhost'
in /etc/hosts.allow provides the minimum necessary access.  Refer to the
manual page `hosts_access(5)' for more information on access controls.


	Additionally the dictd configuration file, /etc/dictd/dictd.conf,
must allow access to dictd by inetd.  If there is no access specification in
the file, there is no restriction on access to the dictd daemon.  If there is
any access specification present, the following line must be present to allow
access by inetd:
          allow inetd
This line may be in addition to other "allow xxx" lines.  The default
configuration file shipped in the Debian dictd package already contains
the line.
